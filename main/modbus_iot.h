#ifndef __MODBUS_IOT_H
#define __MODBUS_IOT_H
#include "mbcontroller.h"
#include "sdkconfig.h"

#define MB_PORT_NUM     2   // Number of UART port used for Modbus connection
#define MB_DEV_SPEED    115200  // The communication speed of the UART
#define SLAVE_ADDRESS 1

#define MODE_SINGLE_READ     0x03
#define MODE_SINGLE_WRITE    0x06
#define MODE_MULTI_WRITE     0x10 

#define CONFIG_MB_UART_RXD 16//22//16
#define CONFIG_MB_UART_TXD 17//23//17
#define CONFIG_MB_UART_RTS 18
typedef struct 
{
    uint8_t param_key[20];          /*!< The key (name) of the parameter */
    mb_param_request_t param;
    int data[10];
}modbus_info_t;

esp_err_t master_init(uart_port_t port_num,uint32_t baud_rate,uart_parity_t parity);
int master_send_request(mb_param_request_t modbus_inf,void *data );
#endif