/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h" 
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "wifi_iot.h"
#include "http_server_app.h"
#include "uart_iot.h"
#include <json_generator.h>
#include <json_parser.h>
#include "modbus_iot.h"
#include "mqtt_iot.h"
#include <string.h>
TimerHandle_t xTimer1;
modbus_info_t modbus_inf[]=
{
//  chuc nang,  dia chi slave, func,
    {"read tan so",{SLAVE_ADDRESS,MODE_SINGLE_READ,200,1},{0}},   //read dong dien 
    {"dong dien",{SLAVE_ADDRESS,MODE_SINGLE_READ,201,1},{0}},   //read dien ap
    {"dien ap",{SLAVE_ADDRESS,MODE_SINGLE_READ,202,1},{0}},   //read tan so
    {"write tan so",{SLAVE_ADDRESS,MODE_SINGLE_WRITE,13,1},{0}},  //write tan so
    {"on-off inv",{SLAVE_ADDRESS,MODE_SINGLE_WRITE,8,1},{0}},
    {"Pr0-Pr8",{SLAVE_ADDRESS,MODE_MULTI_WRITE,1000,9},{0}},  //
};

#define TYPE_UPDATE_HOME    0
#define TYPE_SETTING_BASE   1
#define TYPE_SETTING_PID    2
#define TYPE_SETTING_OTHER  3

#define MODE_SETTING_FREQ   0
#define MODE_SETTING_STATE  1


//data
uint8_t INV_NUM_MAX =2;
uint8_t slave_id_list[]={1,2,3,4,5,6,7,8};

//
enum
{   
    R_FREQUENCY,
    R_CURRENT,
    R_VOLTS,
    W_FREQUENCY,
    W_STATE,
    PR0_8,
};

typedef struct {
    char buf[256];
    size_t offset;
} json_gen_test_result_t;
static void flush_str(char *buf, void *priv)
{
    json_gen_test_result_t *result = (json_gen_test_result_t *)priv;
    if (result) {
        if (strlen(buf) > sizeof(result->buf) - result->offset) {
            printf("Result Buffer too small\r\n");
            return;
        }
        memcpy(result->buf + result->offset, buf, strlen(buf));
        result->offset += strlen(buf);
    }
}

static int str_json_gen(json_gen_test_result_t *result,int type,uint16_t *data,int len)
{
    //json_gen_test_result_t result;
    // char  expected[100];
	char buf[20];
    memset(result, 0, sizeof(json_gen_test_result_t));
	json_gen_str_t jstr;
	json_gen_str_start(&jstr, buf, sizeof(buf), flush_str, result);
	json_gen_start_object(&jstr);
   
	json_gen_obj_set_int(&jstr, "type", type);
    json_gen_push_array(&jstr, "data");
    for (int i=0;i<len;i++)
    { 
        json_gen_arr_set_int(&jstr, data[i]);
    }
	json_gen_pop_array(&jstr);
	json_gen_end_object(&jstr);
	json_gen_str_end(&jstr);
    printf("json:   %s\n",result->buf);
   return 0;
  
}

/*
object={
    "INV_NUM_MAX": 10,
    "VCF":[
        {
            "v":10,
            "c":20,
            "f":30
        },
        
    {10,20,30}]
}
object.VCF[0].v
*/
static esp_err_t get_home_callback(httpd_req_t *req)
{
     char resp_str[100];//modbus_inf[i].data
    json_gen_test_result_t *result;
    // char  expected[100];
    result=(json_gen_test_result_t*)malloc(sizeof(json_gen_test_result_t));
	char buf[20];
    memset(result, 0, sizeof(json_gen_test_result_t));
	json_gen_str_t jstr;
	json_gen_str_start(&jstr, buf, sizeof(buf), flush_str, result);
	json_gen_start_object(&jstr);

    json_gen_obj_set_int(&jstr, "type",TYPE_UPDATE_HOME);
	json_gen_obj_set_int(&jstr, "INV_NUM_MAX",INV_NUM_MAX );
    json_gen_push_array(&jstr, "VCF");
    for (int i=0;i<INV_NUM_MAX;i++)
    {
       	json_gen_start_object(&jstr);

        json_gen_obj_set_int(&jstr,"volts",modbus_inf[R_VOLTS].data[i]);
        json_gen_obj_set_int(&jstr,"current",modbus_inf[R_CURRENT].data[i]);
        json_gen_obj_set_int(&jstr,"frequency",modbus_inf[R_FREQUENCY].data[i]);
        json_gen_end_object(&jstr);
    }
	json_gen_pop_array(&jstr);
	json_gen_end_object(&jstr);
	json_gen_str_end(&jstr);
     
    printf("%s\n",result->buf);

   // sprintf(resp_str,"{\"type\": 0,\"volts\": %d,\"current\":%d,\"frequency\": %d}",modbus_inf[R_VOLTS].data,modbus_inf[R_CURRENT].data,modbus_inf[R_FREQUENCY].data);
    httpd_resp_send(req,(const char*) result->buf, result->offset);
    free(result);
    return ESP_OK;
}

static esp_err_t post_setting_inverter_callback(httpd_req_t *req)
{
    char buf[200];
  //  char string[200];
    httpd_req_recv(req, buf,MIN(req->content_len, sizeof(buf)));
    printf("data: %s\n",buf);

    uint8_t type,addr_id;
    int size_buf;
    uint16_t data_base[9];
    jparse_ctx_t jctx;
	int ret = json_parse_start(&jctx, buf, MIN(req->content_len, sizeof(buf)));
	if (ret != OS_SUCCESS) {
		printf("Parser failed\n");
		return -1;
	}

	json_obj_get_int(&jctx, "type", &type);
    json_obj_get_int(&jctx, "addr_id", &addr_id);
	json_obj_get_array(&jctx, "data", &size_buf);
    printf("Array has %d elements\n", size_buf);
    for (int i = 0; i < size_buf; i++) 
        {
            char ch[5];
            json_arr_get_string(&jctx, i,ch,sizeof(ch));
            data_base[i]=(uint16_t)atoi(ch);
            printf("index %d: %d\n", i, data_base[i]);
        }
        json_obj_leave_array(&jctx);

	json_parse_end(&jctx);

    printf("type: %d\n",type);
    for (int i=0;i<size_buf;i++ )
    {
        printf("a[%d]=%d\n",i,data_base[i]);
    }
    modbus_inf[PR0_8].param.slave_addr=addr_id;
    modbus_inf[PR0_8].param.command=MODE_MULTI_WRITE;
    int error=master_send_request(modbus_inf[PR0_8].param,data_base);
    if (!error)
    {
    printf("write Pr0-Pr8 successful.\n");
    }
    else 
    printf("write Pr0-Pr8 ERROR %d\n",error);

    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK; 
}

static esp_err_t post_control_callback(httpd_req_t *req)
{
    char buf[200];
   // char string[200];
    int mode=0,slave_id=0;
    httpd_req_recv(req, buf,MIN(req->content_len, sizeof(buf)));
    printf("data: %s\n",buf);

    jparse_ctx_t jctx;
	int ret = json_parse_start(&jctx, buf, MIN(req->content_len, sizeof(buf)));
	if (ret != OS_SUCCESS) {
		printf("Parser failed\n");
		return -1;
	}
	json_obj_get_int(&jctx, "MODE", &mode);
    printf("mode: %d\n",mode);
	json_obj_get_int(&jctx, "SLAVE_ID", &slave_id);
    printf("slave id: %d\n",slave_id);

    switch (mode)
    {
    case MODE_SETTING_FREQ:
    {
        int freq_setting=0;
        json_obj_get_int(&jctx, "FREQ", &freq_setting);
        printf("DATA RAW: freq setting %d\n",freq_setting);
        modbus_inf[W_FREQUENCY].param.slave_addr=slave_id;
    int error=master_send_request(modbus_inf[W_FREQUENCY].param,&freq_setting);
    if (!error)
    {
       // modbus_inf[W_FREQUENCY].data=freq_setting;
        printf(" write freq : %d.\n",freq_setting);
    }
    else 
        printf(" ERROR  %d\n",error);
         break;
    }

    case MODE_SETTING_STATE:
    {
        int state_invt=0;
        json_obj_get_int(&jctx, "CONTROL", &state_invt);
        

    printf("DATA RAW: state  %d\n",state_invt);
    //  uint32_t ptr=0;
     modbus_inf[W_STATE].param.slave_addr=slave_id;
    int error=master_send_request(modbus_inf[W_STATE].param,&state_invt);
    if (!error)
    {
    // modbus_inf[W_STATE].data=ptr;
    //modbus_inf[W_STATE].data=state_invt;
    printf(" write state : %s.\n",(state_invt==0)?"OFF":(state_invt==2)?"FWD":"REV");
    }
    else 
    printf(" ERROR  %d\n",error);

        break;
    }
        
    default:
        break;
    }
    
    json_parse_end(&jctx);
   

    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK; 

}

void task_get_VCF()
{
    printf("task\n");
    while (true)
    {
    for (int j=0;j<INV_NUM_MAX;j++)
    {
        for (int i=0; i<3;i++)
        {
            uint32_t ptr=0;
            modbus_inf[i].param.slave_addr=slave_id_list[j];
            int error=master_send_request(modbus_inf[i].param,&ptr);
            if (!error)
            {
            modbus_inf[i].data[j]=ptr;
            printf(" %d read task %d successful.\n",modbus_inf[i].data[j],i);
            }
            else 
            printf(" ERROR %d: %d\n",i,error);  
        }
    }
    
    vTaskDelay(500);
    }
}

static esp_err_t get_update_callback(httpd_req_t *req)
{
    json_gen_test_result_t result;
    uint16_t data_base[9];
    modbus_inf[PR0_8].param.command=MODE_SINGLE_READ;
    master_send_request(modbus_inf[PR0_8].param,data_base);
    str_json_gen(&result,1,data_base,9);
    httpd_resp_send(req,(const char*) result.buf, strlen(result.buf));
    return ESP_OK;
}

// void add_client_mqtt(esp_mqtt_client_handle_t client)
// {
//     esp_mqtt_client_subscribe(client, "/dylan/monitor", 0);
//     esp_mqtt_client_subscribe(client, "/dylan/setting", 0);
// }
// void mqtt_event_data_callback(char *topic,char *data)
// {
//     printf("topic=%s\n",topic);
//     if (strcmp(topic,"/dylan/monitor")==0)
//     {
//          printf("data=%s\n",data);
//     }
//     if (strcmp(topic,"/dylan/setting")==0)
//     {
//          printf("data=%s\n",data);
//     }
// }

void app_main(void)
{
    wifi_init();
    master_init(2,9600,UART_PARITY_EVEN);
    app_config(PROVISION_SMARTCONFIG);
    start_webserver("/");
    http_add("/home",HTTP_GET,get_home_callback);
    http_add("/update",HTTP_GET,get_update_callback);
    http_add("/setting",HTTP_POST,post_setting_inverter_callback);
    http_add("/control",HTTP_POST,post_control_callback);

 /*
    json_gen_test_result_t result;
    str_json_gen(&result,10,20,30);
    printf("%s\n",result.buf);
    //json_pare_setting_data(result.buf,result.ofset);
    modbus_data[R_VOLTS]=5;
    modbus_data[R_CURRENT]=10;
    modbus_data[R_FREQUENCY]=15;
     // mqtt_app_start();
    // mqtt_client_subscribe(add_client_mqtt);
    // mqtt_even_data(mqtt_event_data_callback);
*/
    
    xTaskCreate(task_get_VCF," get VCF",1024*2,NULL,5,NULL);


    while (true)
    {
        vTaskDelay(100);
    }
    
}
