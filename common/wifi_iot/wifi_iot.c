#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "wifi_iot.h"
#include "esp_netif.h"
#include "esp_wifi_netif.h"
#include "esp_smartconfig.h"
#include "http_server_app.h"
#define EXAMPLE_ESP_MAXIMUM_RETRY  3
#define EXAMPLE_MAX_STA_CONN       5


/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t s_wifi_event_group;

static const int WIFI_CONNECTED_BIT=BIT0;
static const int WIFI_FAIL_BIT     =BIT1;
static const int ESPTOUCH_DONE_BIT =BIT2;
static const int ESPTOUCH_OK_BIT =BIT3;
static const int HTTP_DONE_BIT =BIT3;

static const char *TAG = "wifi station";

static int s_retry_num = 0;

static TaskHandle_t xHandle=NULL;
static void event_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } 
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) 
    {
        if (s_retry_num < EXAMPLE_ESP_MAXIMUM_RETRY) 
        {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG, "retry to connect to the AP");
        }
        else 
        {
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
            xEventGroupClearBits(s_wifi_event_group,WIFI_CONNECTED_BIT);
            vTaskResume (xHandle);
            ESP_LOGI(TAG,"connect to the AP fail");
        }
        
    } 
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) 
    {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
    else if (event_id == WIFI_EVENT_AP_STACONNECTED) 
    {
        wifi_event_ap_staconnected_t* event = (wifi_event_ap_staconnected_t*) event_data;
        ESP_LOGI(TAG, "station "MACSTR" join, AID=%d",
                 MAC2STR(event->mac), event->aid);
    } 
    else if (event_id == WIFI_EVENT_AP_STADISCONNECTED) 
    {
        wifi_event_ap_stadisconnected_t* event = (wifi_event_ap_stadisconnected_t*) event_data;
        ESP_LOGI(TAG, "station "MACSTR" leave, AID=%d",MAC2STR(event->mac), event->aid);
    }
    else if (event_base == SC_EVENT && event_id == SC_EVENT_SCAN_DONE) 
    {
        ESP_LOGI(TAG, "Scan done");
    } 
    else if (event_base == SC_EVENT && event_id == SC_EVENT_FOUND_CHANNEL) 
    {
        ESP_LOGI(TAG, "Found channel");
    } 
    else if (event_base == SC_EVENT && event_id == SC_EVENT_GOT_SSID_PSWD) 
    {
        ESP_LOGI(TAG, "Got SSID and password");
        smartconfig_event_got_ssid_pswd_t *evt = (smartconfig_event_got_ssid_pswd_t *)event_data;
        wifi_config_t wifi_config;
        uint8_t ssid[32] = { 0 };
        uint8_t password[64] = { 0 };
        uint8_t rvd_data[33] = { 0 };

        bzero(&wifi_config, sizeof(wifi_config_t));
        memcpy(wifi_config.sta.ssid, evt->ssid, sizeof(wifi_config.sta.ssid));
        memcpy(wifi_config.sta.password, evt->password, sizeof(wifi_config.sta.password));
        wifi_config.sta.bssid_set = evt->bssid_set;
        if (wifi_config.sta.bssid_set == true) 
        {
            memcpy(wifi_config.sta.bssid, evt->bssid, sizeof(wifi_config.sta.bssid));
        }

        memcpy(ssid, evt->ssid, sizeof(evt->ssid));
        memcpy(password, evt->password, sizeof(evt->password));
        ESP_LOGI(TAG, "SSID:%s", ssid);
        ESP_LOGI(TAG, "PASSWORD:%s", password);
                if (evt->type == SC_TYPE_ESPTOUCH_V2) {
            ESP_ERROR_CHECK( esp_smartconfig_get_rvd_data(rvd_data, sizeof(rvd_data)) );
            ESP_LOGI(TAG, "RVD_DATA:");
            for (int i=0; i<33; i++) {
                printf("%02x ", rvd_data[i]);
            }
            printf("\n");
        }
        ESP_ERROR_CHECK( esp_wifi_disconnect() );
        ESP_ERROR_CHECK( esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
        esp_wifi_connect();
        xEventGroupSetBits(s_wifi_event_group, ESPTOUCH_OK_BIT);
    } 
    else if (event_base == SC_EVENT && event_id == SC_EVENT_SEND_ACK_DONE) 
    {
        xEventGroupSetBits(s_wifi_event_group, ESPTOUCH_DONE_BIT);
        ESP_LOGI(TAG, "smart config done");
    }
}
void wifi_disconnect_task( )
{
    while (1)
    {
    vTaskSuspend(xHandle);
    // EventBits_t bits=xEventGroupWaitBits(s_wifi_event_group,WIFI_FAIL_BIT, pdTRUE,pdFALSE,portMAX_DELAY);
    // if ((bits & WIFI_FAIL_BIT))
    // {
    // ESP_LOGI(TAG, "wifi_disconnect_task");
    s_retry_num=0;
    int provisition=1;
    if(provisition == PROVISION_SMARTCONFIG)
        {
            ESP_ERROR_CHECK(esp_wifi_start());
            // EventBits_t uxBits;
            ESP_ERROR_CHECK( esp_smartconfig_set_type(SC_TYPE_ESPTOUCH) );
            smartconfig_start_config_t cfg = SMARTCONFIG_START_CONFIG_DEFAULT();
            ESP_ERROR_CHECK( esp_smartconfig_start(&cfg) ); 
            xEventGroupWaitBits(s_wifi_event_group, ESPTOUCH_OK_BIT, true, true, portMAX_DELAY);
            xEventGroupWaitBits(s_wifi_event_group, ESPTOUCH_DONE_BIT, true, true, 10000/portTICK_RATE_MS); 
            esp_smartconfig_stop();         
        }
        else if(provisition == PROVISION_ACCESSPOINT)
        {
            wifi_init_softap("DEVIOT DEMO","123456789");
        }
    }
    // }
    
}

void wifi_init(void)
{
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) 
    {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    s_wifi_event_group = xEventGroupCreate();
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_sta();
    esp_netif_create_default_wifi_ap();
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    ESP_ERROR_CHECK( esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL) );
    ESP_ERROR_CHECK( esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL) );
   
    xTaskCreate(wifi_disconnect_task,"connected fail",1024,NULL,3,&xHandle);
}

void wifi_disconnect(void)
{
    esp_wifi_disconnect();
    esp_wifi_stop();
}
int wifi_init_sta(char *wifi_ssid,char *wifi_pass)
{
    wifi_config_t wifi_config = {
        .sta = {
	     .threshold.authmode = WIFI_AUTH_WPA2_PSK,

            .pmf_cfg = {
                .capable = true,
                .required = false
            },
        },
    };
    strcpy((char *)wifi_config.sta.ssid,wifi_ssid);
    strcpy((char *)wifi_config.sta.password,wifi_pass);
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

    // ESP_LOGI(TAG, "wifi_init_sta finished.");

    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    if (bits & WIFI_CONNECTED_BIT) {
        ESP_LOGI(TAG, "connected to ap SSID:%s password:%s", wifi_ssid, wifi_pass);
        return 1;
    } 
    else if (bits & WIFI_FAIL_BIT) 
    {
        ESP_LOGI(TAG, "Failed to connect to SSID:%s, password:%s",wifi_ssid, wifi_pass);
        // return 0;
    }
    return 0;
}

void wifi_init_softap(char* AP_ssid,char* AP_pass)
{
    wifi_config_t wifi_config = {
        .ap = {
            // .ssid = AP_ssid,
            .ssid_len = strlen(AP_ssid),
            .channel = 0,
            // .password = AP_pass,
            .max_connection = EXAMPLE_MAX_STA_CONN,
            .authmode = WIFI_AUTH_WPA_WPA2_PSK
        },
    };
    strcpy((char *)wifi_config.sta.ssid,AP_ssid);
    strcpy((char *)wifi_config.sta.password,AP_pass);
    if (strlen(AP_ssid) == 0) {
        wifi_config.ap.authmode = WIFI_AUTH_OPEN;
    }

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
    
    ESP_LOGI(TAG, "wifi_init_softap finished. SSID:%s password:%s channel:%d",
    wifi_config.ap.ssid,  wifi_config.ap.password,  wifi_config.ap.channel);
}


bool is_provisioned(void)
{
    // wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    // ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    bool provisioned=false;
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));

    wifi_config_t wifi_config;
    esp_wifi_get_config(WIFI_IF_STA, &wifi_config);

    if(wifi_config.sta.ssid[0] != 0x00)
    {
        provisioned = true;
    }
    return provisioned;
}

static esp_err_t wifi_info_handler(httpd_req_t *req)
{
    char buf[100];
    httpd_req_recv(req, buf,MIN(req->content_len, sizeof(buf)));
    //"Deviot2|12345679|"
    char ssid[30] = "";
    char pass[30] = "";
    printf("->data: %s\n", buf);
    char *pt = strtok(buf, "|");
    if(pt)
        strcpy(ssid, pt);
    pt = strtok(NULL, "|");
    if(pt)    
        strcpy(pass, pt);

    printf("->ssid: %s\n", ssid);
    printf("->pass: %s\n", pass);

    wifi_disconnect();
    wifi_init_sta(ssid,pass);
    xEventGroupSetBits(s_wifi_event_group, HTTP_DONE_BIT);
    return ESP_OK;
}

void app_config(provision_type_t provisition_type)
{
     ESP_ERROR_CHECK( esp_event_handler_register(SC_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL) );
    bool provisioned = is_provisioned();    // check xem wifi cấu hình chưa
    if(!provisioned)
    {
        if(provisition_type == PROVISION_SMARTCONFIG)
        {
            ESP_ERROR_CHECK(esp_wifi_start());
            // EventBits_t uxBits;
            ESP_ERROR_CHECK( esp_smartconfig_set_type(SC_TYPE_ESPTOUCH) );
            smartconfig_start_config_t cfg = SMARTCONFIG_START_CONFIG_DEFAULT();
            ESP_ERROR_CHECK( esp_smartconfig_start(&cfg) );   
            xEventGroupWaitBits(s_wifi_event_group, ESPTOUCH_OK_BIT, true, true, portMAX_DELAY);
            xEventGroupWaitBits(s_wifi_event_group, ESPTOUCH_DONE_BIT, true, true,10000/portTICK_PERIOD_MS);
            esp_smartconfig_stop();
        }
        else if(provisition_type == PROVISION_ACCESSPOINT)
        {
            wifi_init_softap("DEVIOT DEMO","123456789");
            start_webserver("/");
            http_add("/wifiinfo",HTTP_POST,wifi_info_handler);
            xEventGroupWaitBits(s_wifi_event_group, HTTP_DONE_BIT, false, true, portMAX_DELAY); 
            stop_webserver();

        }
    }
    else
    {
        ESP_ERROR_CHECK(esp_wifi_start());
    }

    xEventGroupWaitBits(s_wifi_event_group, WIFI_CONNECTED_BIT, false, true, portMAX_DELAY);    
    ESP_LOGI(TAG, "wifi connected"); 
    }



