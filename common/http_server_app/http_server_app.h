#ifndef  __HTTP_SERVER_APP_H
#define __HTTP_SERVER_APP_H
#include <esp_event.h>
#include <esp_log.h>
#include <esp_system.h>
#include <sys/param.h>
#include "esp_netif.h"
#include "esp_eth.h"
#include "string.h"
#include <esp_http_server.h>
#include <driver/gpio.h>
typedef esp_err_t (*uri_handle_t) (httpd_req_t);
typedef void (*data_uri_handle_callback_t) (char*,int);
void start_webserver(const char *url);
void stop_webserver(void);
void http_add(const char *url,httpd_method_t method,void *callback);

#endif