#ifndef UART_IO_H
#define UART_IO_H
#include "esp_err.h"
#include "hal/gpio_types.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"
#include "string.h"
#include "driver/uart.h"

#define BIT_UART0    ( 1 << 10 )
#define BIT_UART1    ( 1 << 11 )
#define BIT_UART2    ( 1 << 12 )
#define BUFFER_DATA  1024
#define UART_NUM_MAXS 3

QueueHandle_t  uart_queue[UART_NUM_MAXS];
EventGroupHandle_t xEventGroup;
typedef void (*uart_callback_t)(void);

void uart_iot_write(uart_port_t uart_num, const void * src, size_t size);
int uart_iot_read(uart_port_t uart_num, void* buf);
void uart_iot_init(uart_port_t uart_num, int baund_rate1);
void uart_set_callback(uart_port_t uart_num,void *cb);
void uart_reset_callback(uart_port_t uart_num);
#endif