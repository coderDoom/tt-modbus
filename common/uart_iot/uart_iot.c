#include <stdio.h>
#include <esp_log.h>
#include <driver/uart.h>
#include "freertos/task.h"

#include "freertos/event_groups.h"
#include "uart_iot.h"
#include "string.h"

uart_callback_t uart_callback[UART_NUM_MAX];


void task_wait_uart0()
{
    uart_event_t event;

    for(;;)
    {
        if(xQueueReceive(uart_queue[UART_NUM_0], (void * )&event, (portTickType)portMAX_DELAY))
        {
            switch(event.type)
            {
                case UART_DATA:
                    xEventGroupSetBits(xEventGroup,BIT_UART0);                
                    break;

                default:
                    break;
            }
        }
    }
    
}
void task_wait_uart1()
{
    uart_event_t event;

    for(;;)
    {
        if(xQueueReceive(uart_queue[UART_NUM_1], (void * )&event, (portTickType)portMAX_DELAY))
        {
            switch(event.type)
            {
                case UART_DATA:
                    xEventGroupSetBits(xEventGroup,BIT_UART0);                
                    break;

                default:
                    break;
            }
        }
    }
    
}
void task_wait_uart2()
{
    uart_event_t event;

    for(;;)
    {
        if(xQueueReceive(uart_queue[UART_NUM_2], (void * )&event, (portTickType)portMAX_DELAY))
        {
            switch(event.type)
            {
                case UART_DATA:
                    xEventGroupSetBits(xEventGroup,BIT_UART0);                
                    break;

                default:
                    break;
            }
        }
    }
    
}

void Task_event_handler()
{
     

        while(true)
        {

        EventBits_t    uxBits = xEventGroupWaitBits (xEventGroup,  BIT_UART0 | BIT_UART2 | BIT_UART1, pdTRUE,pdFALSE, portMAX_DELAY);

            if (uxBits & BIT_UART0)
            {
                uart_callback[UART_NUM_0]();
            }
            if (uxBits & BIT_UART1)
            {
                uart_callback[UART_NUM_1]();
            }
            if (uxBits & BIT_UART2)
            {
                uart_callback[UART_NUM_2]();
            }
            

        }
}
void uart_iot_write(uart_port_t uart_num, const void * src, size_t size)
{
    uart_write_bytes(uart_num,src,size);
}

int uart_iot_read(uart_port_t uart_num, void* buf)
{
    uint8_t * dtmp=(uint8_t *)malloc(1024);
    int BUF=1024;
    int len;
    len=uart_read_bytes(uart_num,dtmp,BUF,0);
    memcpy(buf,dtmp,len);
   return len;
}

void uart_iot_init(uart_port_t uart_num, int baund_rate1)
{
      uart_config_t uart_config = {
        .baud_rate = baund_rate1,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_APB,
    };

    uart_param_config(uart_num,&uart_config);
    uart_set_pin(uart_num,UART_PIN_NO_CHANGE,UART_PIN_NO_CHANGE,UART_PIN_NO_CHANGE,UART_PIN_NO_CHANGE);
    uart_driver_install(uart_num,BUFFER_DATA*2,BUFFER_DATA*2,20, &uart_queue[uart_num],0);
    uart_callback[uart_num]=NULL;
    xEventGroup=xEventGroupCreate();

    switch (uart_num)
    {
        case UART_NUM_0:
            xTaskCreate(task_wait_uart0,"wait for uart0",1024,NULL,5,NULL);
            break;
        
        case UART_NUM_1:
            xTaskCreate(task_wait_uart1,"wait for uart0",1024,NULL,5,NULL);
            break;
        
        case UART_NUM_2:
            xTaskCreate(task_wait_uart2,"wait for uart0",1024,NULL,5,NULL);
            break;
        
        default:
            break;

    }
    
    xTaskCreate(Task_event_handler,"event handle",1024*2,NULL,6,NULL);   
    
}

void uart_set_callback(uart_port_t uart_num,void *cb)
{
    uart_callback[uart_num]=cb;
}

void uart_reset_callback(uart_port_t uart_num)
{
    uart_callback[uart_num]=NULL;
}

